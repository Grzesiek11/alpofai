# A Linux Port of Fire and Ice

This is an unofficial Linux port of [A Dance of Fire and Ice (ADOFAI)](https://store.steampowered.com/app/977950/A_Dance_of_Fire_and_Ice/).

We believe that ADOFAI is a great rhythm game. However, despite the game being developed in Unity, which allows the developer to easily port it to all platforms, there is no Linux build of it available.

Running the game through Wine/Proton has a negative impact on performance, especially on harder user created levels. It's an option, but it isn't ideal.

We aim to fix that. Or at least, provide a workaround. The real fix would be 7th Beat Games making an official port.

**This project is experimental.** Things *will* break. Check [this section](#what-works-and-what-doesnt) on details.

## Project status

7th Beat Games made an official Linux port of ADOFAI!

![](resources/announcment.png)

This project is no longer needed, so I'm closing all issues and archiving it. It was broken for a while anyways, due to ADOFAI bumping the Unity version (I wanted to fix this, but didn't have the time to). Go download the game from Steam and play!

It is funny how the same issue of video not working (#2) affects the official port (for now) though. I read it's a general Unity issue.

Also, as this is the last commit, it doesn't hurt to add that @huantian, who helped with testing and debugging this project, works at 7th Beat Games now!

## Legal

**A Linux Port of Fire and Ice** is not affiliated, associated, authorized, endorsed by, or in any way officially connected with **A Dance of Fire and Ice** or **7th Beat Games**.

**To copyright holders:** This repository does **not** contain any copyrighted material and does **not** allow for piracy. Users need to obtain the game before generating the unofficial port.

## What works (and what doesn't)

More testing is required! If you have any issues or suggestions, please, let us know on the [Issues](https://gitlab.com/grzesiek11/alpofai/-/issues) page.

### Scripts

#### Working

- Successfully builds ALPOFAI on at least 3 different systems

#### Known issues

- `steamcmd` downloads the OST with the game if you own it, which is a waste of disk space and time
- `steamcmd` seems to download the Alpha build of ADOFAI for some users. Might depend on whether or not the user activated Alpha in the Steam client.
- `steamcmd` can't download the game if it's borrowed from another user

### Gameplay

#### Working

- Game boots
- All official levels
- Editor
- Steam overlay **if the port is launched through Steam** (as a non-Steam game)
- Downloading levels from the Steam Workshop and playing them
- Discord RPC

#### Known issues

- Video backgrounds don't work at all and are replaced by a black background (#2)
- One user reported the framerate locking at 60 FPS
- The hardcoded filter in SFB causes file dialogs to always use `.adofai` by default

### Future features

- Easier installation process and better instructions
- Configuration separate to scripts
- Download of Alpha and Beta versions of ADOFAI
- Checking requirements against checksums, so they aren't redownloaded for no reason
- Issue template

## How to use

1. Install dependencies using your package manager: `steamcmd`, `wget`, `p7zip-full` and `unzip`. Package names can vary from distribution to distribution, these are used in Debian-based ones.
2. Edit the `common` file and set `steam_user` and `steam_password` to your Steam username and password. **The account needs to have ADOFAI in its library.**
3. Run the `download_requirements` script to download ADOFAI, Unity runtimes and libraries.
4. Run the `build` script to generate ALPOFAI.
5. Go to the `dist` folder. If everything went well, it contains a generated and (hopefully) working ALPOFAI. Run the `ALPOFAI.desktop` file or the `ALPOFAI` binary to start the game. **Steam must be running for Steam API to work.**
6. You can add the port to Steam as a non-Steam game to fix the Steam overlay. Open Steam, go to `Library`, `Add a game`, `Add a Non-Steam Game` and select the `ALPOFAI.desktop` file from the `dist` folder.

## Additional info

- You can copy the `ALPOFAI.desktop` file to `~/.local/share/applications` to add it to your application launcher
- Files from `C:\Users\[Username]\AppData\LocalLow\7th Beat Games\A Dance of Fire and Ice` (including logs) are in `~/.config/unity3d/7th Beat Games/A Dance of Fire and Ice`
- Save files are stored in the `dist/User` folder

## How does it work?

**Does it use Wine?** No. Not at all.

**Is it native?** Yes! Or at least, kind of. Well, it's the same amount of native as it is on Windows and macOS.

Unity projects don't compile to native bytecode binaries. Instead, it's compiled to C# bytecode and some Unity blobs. These can be reused on any platform, provided the right set of runtimes and native libraries. That's what Unity does to compile its projects to multiple platforms without changing too much code.

That's what we do here as well. It requires some hacks and isn't perfect, but it's a "native" (Unity is not really native on any platform) way to run the game on Linux.

We use the macOS build of the game as a base (since it supports OpenGL, as opposed to the Windows build which supports DirectX only) and provide the required runtimes and native libraries.

You can look at the implementation details in the scripts' sources.

## Compiling the SFB (Standalone File Browser) library

1. Install dependencies using your package manager: `gcc`, `cmake`, `pkgconf`, `make` and `libgtk-3-dev`. Package names can vary from distribution to distribution, these are used in Debian-based ones.
2. Go to the `standalone_file_browser` directory:

    ```sh
    cd standalone_file_browser
    ```
3. Create a `build` directory and go into it:

    ```sh
    mkdir build && cd build
    ```

4. Configure the project:

    ```sh
    cmake ..
    ```

5. Compile the shared library:

    ```sh
    make -j $(nproc)
    ```

## Code used

[StandaloneFileBrowser](https://github.com/gkngkc/UnityStandaloneFileBrowser) - for a modified version that fixes ADOFAI bugs
