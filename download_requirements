#!/bin/sh

. "$(readlink -f "$(dirname "$0")")/common"

# Links
# Unity runtime for Linux
# SHA-256: fd8b5b42da3b11910a45915935705cf93248c64d84d95c426d7db895129ff243
# 2019.3.0f6 is used by ADOFAI as of 1.13.0
# TODO: Getting this link could be automated to only needing the ID (here 27ab2135bccf) using the .ini files provided. Maybe we can somehow figure out the ID based on the level0 version too...?
unity_link='https://download.unity3d.com/download_unity/27ab2135bccf/TargetSupportInstaller/UnitySetup-Linux-Mono-Support-for-Editor-2019.3.0f6.exe'
# Discord RPC library
# SHA-256: dac1f5dc6bedaeab1cc3c2c7fd4261e00838c81619c3ee325f3723c3d55ee03a
# Latest version (3.4.0) works, it won't be getting any updates as it is discontinued
discord_rpc_link='https://github.com/discord/discord-rpc/releases/download/v3.4.0/discord-rpc-linux.zip'
# Steam API library
# SHA-256: 6b5b7b260ff925b216dfb3643131c4da9d6a5ddd8c5c8f44beed85ac09be62a4
# I have no idea what is the source of this file. Different versions/compilations of it come with most Linux games, I got mine from Patrick's Parabox (1260520). The one from Celeste (504230) refused to boot. Needs testing.
# I don't know if I can redistribute this. I host it on my server, so I can remove it without any hassle.
steam_api_link='https://grzesiek11.stary.pc.pl/files/stuff/libsteam_api.so'
# StandaloneFileBrowser library, compiled from the standalone_file_browser folder of this repo
# SHA-256: ef49060485464326cc2d601cb1b6d9c3bb4a0c30824f49e0d575e22c5be76dfb
standalone_file_browser_link='https://grzesiek11.stary.pc.pl/files/stuff/libStandaloneFileBrowser.so'

mkdir -p "$req_dir"

# Download macOS version of ADOFAI from Steam
if [ -z "$steam_user" ] || [ -z "$steam_password" ]; then
    if [ -d "$req_dir/adofai" ]; then
        printf '[WARN] The Steam username and/or password is empty, but %s/adofai exists, so it will be used.\n' "$req_dir"
    else
        printf '[ERROR] The Steam username and/or password is empty. Please input your credentials in the common file.\n'
        exit 1
    fi
else
    # TODO: Allow for download of beta and alpha versions (-beta and -betapassword flags to app_update)
    steamcmd +@sSteamCmdForcePlatformType macos +login "$steam_user" "$steam_password" +force_install_dir "$req_dir/adofai" +app_update "$steam_appid" validate +quit || { printf '[ERROR] steamcmd failed. Maybe the credentials are invalid or you don'\''t have ADOFAI in your library?\n' && exit 1; }
fi

# Download the Unity runtime
wget -O "$req_dir/unity_runtime.exe" "$unity_link" || { printf '[ERROR] Can'\''t download the Unity runtime.\n' && exit 1; }

# Download plugins
# libdiscord-rpc.so
wget -O "$req_dir/discord_rpc.zip" "$discord_rpc_link" || { printf '[ERROR] Can'\''t download the Discord RPC library.\n' && exit 1; }
# libsteam_api.so
wget -O "$req_dir/libsteam_api.so" "$steam_api_link" || { printf '[ERROR] Can'\''t download the Steam API library.\n' && exit 1; }
# libStandaloneFileBrowser.so
wget -O "$req_dir/libStandaloneFileBrowser.so" "$standalone_file_browser_link" || { printf '[ERROR] Can'\''t download the StandaloneFileBrowser library.\n' && exit 1; }
